/***
 * @Author: smhr 13476512@qq.com
 * @Date: 2022-10-05 09:21:28
 * @LastEditors: smhr 13476512@qq.com
 * @LastEditTime: 2022-10-05 13:11:13
 * @FilePath: test.c
 * @Description:
 * @
 * @Copyright (c) 2022 by smhr 13476512@qq.com, All Rights Reserved.
 */

/***
 * @description:加法函数
 * @param {int} a 数1
 * @param {int} b 数2
 * @return {*} 和
 */
int add(int a, int b)
{
    return a + b;
}

/***
 * @description:主函数
 * @return {*}
 */
int main(void)
{
    {
        do
        {
            //测试下
            add(1, 2);
        } while (1);
    }
    return 1;
}
